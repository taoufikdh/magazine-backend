## Description

Application where a user can subscribe to digital magazines for a monthly
price

## Installation

```bash
$ npm install
```

## Running the app

```bash
# create postgres and pgadmin conatiners
$ docker-compose up
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
