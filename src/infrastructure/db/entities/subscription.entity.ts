import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from 'typeorm';
import { UserEntity } from './user.entity';
import { MagazineEntity } from './magazine.entity';

@Entity()
export class SubscriptionEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => UserEntity)
  user: UserEntity;

  @ManyToOne(() => MagazineEntity)
  magazine: MagazineEntity;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  startDate: Date;

  // Automatically generate the endDate field with a default value for simplicity.
  // In a professional project, it should be defined according to the business requirements.
  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP + INTERVAL 5 DAY',
  })
  endDate: Date;
}
