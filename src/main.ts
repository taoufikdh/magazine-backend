import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

const config = new DocumentBuilder()
  .setTitle('Digital Magazine API')
  .setDescription('API for managing digital magazines subscriptions')
  .setVersion('1.0')
  .addTag('Magazines')
  .build();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // Set global prefix for all API endpoints
  app.setGlobalPrefix('api');

  app.enableCors();
  //swagger setup
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  const configService = app.get(ConfigService);

  const port = configService.get('APPLICATION_PORT') || 3000;
  await app.listen(port);
  console.log(`Application is running on: ${await app.getUrl()}`);
}
bootstrap();
