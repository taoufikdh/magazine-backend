import { DynamicModule, Module, Type } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { MagazineModule } from './modules/magazine/magazine.module';
import { UserModule } from './modules/user/user.module';
import { DbModule } from './infrastructure/db/db.module';
import { SubscriptionModule } from './modules/subscriptions/subscription.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MagazineModule,
    UserModule,
    SubscriptionModule,
    DbModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public static withDatabase(databaseModule: Type<any>): DynamicModule {
    return {
      module: AppModule,
      imports: [databaseModule],
    };
  }
}
