import { ApiProperty } from '@nestjs/swagger';

export class UserInputDTO {
  @ApiProperty()
  name: string;
  @ApiProperty()
  email: string;
}
