import { ApiProperty } from '@nestjs/swagger';
import { UserInputDTO } from '../requests/user.input.dto';

export class UserOutputDTO extends UserInputDTO {
  @ApiProperty()
  id: number;
}
