import { Controller, Post, Body } from '@nestjs/common';
import { ApiTags, ApiResponse } from '@nestjs/swagger';
import { UserOutputDTO } from '../dtos/response/user.output.dto';
import { UserInputDTO } from '../dtos/requests/user.input.dto';
import { UserService } from '../services/user.service';

// Routes are not protected because I haven't implemented the authentication module due to time constraints.
// In case of authentication implementation, an auth guard will be applied before each controller to make them protected.
// TODO A signup and signin endpoints will be implemented and then contol routers regardin to signed in user
@ApiTags('Users')
@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiResponse({ status: 201, description: 'User created successfully.' })
  @Post()
  async createUser(
    @Body() createUserDto: UserInputDTO,
  ): Promise<UserOutputDTO> {
    return this.userService.createUser(createUserDto);
  }
}
