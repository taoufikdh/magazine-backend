// src/user/user.service.ts
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from 'src/infrastructure/db/entities/user.entity';
import { Repository } from 'typeorm';
import { UserOutputDTO } from '../dtos/response/user.output.dto';
import { UserInputDTO } from '../dtos/requests/user.input.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}

  async createUser(createUserDto: UserInputDTO): Promise<UserOutputDTO> {
    const { name, email } = createUserDto;
    const user = new UserEntity();
    user.name = name;
    user.email = email;
    return this.userRepository.save(user);
  }
}
