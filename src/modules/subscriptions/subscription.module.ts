import { Module } from '@nestjs/common';
import { SubscriptionService } from './services/subscription.service';
import { SubscriptionController } from './controllers/subscription.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SubscriptionEntity } from 'src/infrastructure/db/entities/subscription.entity';
import { MagazineEntity } from 'src/infrastructure/db/entities/magazine.entity';
import { UserEntity } from 'src/infrastructure/db/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([SubscriptionEntity, MagazineEntity, UserEntity]),
  ],
  providers: [SubscriptionService],
  controllers: [SubscriptionController],
})
export class SubscriptionModule {}
