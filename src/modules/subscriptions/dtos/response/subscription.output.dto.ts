import { ApiProperty } from '@nestjs/swagger';
import { UserEntity } from 'src/infrastructure/db/entities/user.entity';
import { MagazineOutputDTO } from 'src/modules/magazine/dtos/response/magazine.output.dto';

export class SubscriptionOutputDTO {
  @ApiProperty()
  magazine: MagazineOutputDTO;

  @ApiProperty()
  user: UserEntity;
}
