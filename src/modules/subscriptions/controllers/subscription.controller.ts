// src/subscription/subscription.controller.ts
import { Controller, Get, Param, Post, Delete } from '@nestjs/common';
import { ApiTags, ApiResponse } from '@nestjs/swagger';
import { SubscriptionService } from '../services/subscription.service';
import { SubscriptionOutputDTO } from '../dtos/response/subscription.output.dto';

@ApiTags('Subscriptions')
@Controller('subscriptions')
export class SubscriptionController {
  constructor(private readonly subscriptionService: SubscriptionService) {}
  @ApiResponse({
    status: 200,
    description: 'Return all subscriptions for the user.',
  })
  @Get(':userId')
  async getUserSubscriptions(
    @Param('userId') userId: string,
  ): Promise<SubscriptionOutputDTO[]> {
    return this.subscriptionService.getUserSubscriptions(+userId);
  }

  @ApiResponse({
    status: 200,
    description: 'Return all past subscriptions for the user.',
  })
  @Get('past/:userId')
  async getPastSubscriptions(
    @Param('userId') userId: string,
  ): Promise<SubscriptionOutputDTO[]> {
    return this.subscriptionService.getPastSubscriptions(+userId);
  }
  @ApiResponse({
    status: 201,
    description: 'Successfully subscribed to the magazine.',
  })
  @Post(':userId/:magazineId')
  async subscribe(
    @Param('userId') userId: string,
    @Param('magazineId') magazineId: string,
  ): Promise<SubscriptionOutputDTO> {
    return this.subscriptionService.subscribe(+userId, +magazineId);
  }

  @ApiResponse({
    status: 204,
    description: 'Subscription canceled successfully.',
  })
  @Delete(':subscriptionId/:userId/:magazineId')
  async cancelSubscription(
    @Param('subscriptionId') subId: string,
    @Param('userId') userId: string,
    @Param('magazineId') magazineId: string,
  ): Promise<void> {
    await this.subscriptionService.cancelSubscription(
      +subId,
      +userId,
      +magazineId,
    );
  }
}
