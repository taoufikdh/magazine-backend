// src/subscription/subscription.service.ts
import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MoreThan, Repository } from 'typeorm';
import { SubscriptionOutputDTO } from '../dtos/response/subscription.output.dto';
import { UserEntity } from 'src/infrastructure/db/entities/user.entity';
import { MagazineEntity } from 'src/infrastructure/db/entities/magazine.entity';
import { SubscriptionEntity } from 'src/infrastructure/db/entities/subscription.entity';

@Injectable()
export class SubscriptionService {
  constructor(
    @InjectRepository(SubscriptionEntity)
    private subscriptionRepository: Repository<SubscriptionEntity>,
    @InjectRepository(MagazineEntity)
    private magazineRepository: Repository<MagazineEntity>,
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}

  async getUserSubscriptions(userId: number): Promise<SubscriptionOutputDTO[]> {
    try {
      const user = await this.userRepository.findOneBy({ id: userId });

      if (!user) {
        // Handle case where user is not found
        throw new NotFoundException('User not found');
      }
      return await this.subscriptionRepository.find({
        where: { user: { id: userId } },
        relations: ['user', 'magazine'],
      });
    } catch (error) {
      throw new NotFoundException('User subscriptions not found');
    }
  }

  async getPastSubscriptions(userId: number): Promise<SubscriptionOutputDTO[]> {
    try {
      const currentDate = new Date();
      return this.subscriptionRepository.find({
        where: {
          user: { id: userId },
          endDate: MoreThan(currentDate),
        },
      });
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }
  async subscribe(
    userId: number,
    magazineId: number,
  ): Promise<SubscriptionOutputDTO> {
    try {
      // Fetch the user and magazine entities from the database based on their IDs
      const user = await this.userRepository.findOneBy({ id: userId });
      const magazine = await this.magazineRepository.findOneBy({
        id: magazineId,
      });

      // Handle case where user or magazine is not found
      if (!user || !magazine) {
        throw new NotFoundException('User or magazine not found');
      }
      // create the subscription
      const subscription = new SubscriptionEntity();
      subscription.user = user;
      subscription.magazine = magazine;

      return this.subscriptionRepository.save(subscription);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async cancelSubscription(
    subId: number,
    userId: number,
    magazineId: number,
  ): Promise<void> {
    try {
      // Fetch the user and magazine entities from the database based on their IDs
      const magazine = await this.magazineRepository.findOneBy({
        id: magazineId,
      });
      const user = await this.userRepository.findOneBy({ id: userId });

      if (!user || !magazine) {
        throw new NotFoundException('User or magazine not found');
      }
      await this.subscriptionRepository.delete({ id: subId });
    } catch (error) {
      throw new NotFoundException(error);
    }
  }
}
