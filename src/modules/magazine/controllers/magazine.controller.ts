// src/magazine/magazine.controller.ts
import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Put,
  Delete,
} from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { MagazineInputDTO } from '../dtos/requests/magazine.input.dto';
import { MagazineOutputDTO } from '../dtos/response/magazine.output.dto';
import { MagazineService } from '../services/magazine.service';

@ApiTags('Magazines')
@Controller('magazines')
export class MagazineController {
  constructor(private readonly magazineService: MagazineService) {}

  @ApiOperation({ summary: 'Get all magazines' })
  @ApiResponse({ status: 200, description: 'Return all magazines.' })
  @Get()
  async findAll(): Promise<MagazineOutputDTO[]> {
    return this.magazineService.findAll();
  }

  @ApiOperation({ summary: 'Get a magazine by ID' })
  @ApiResponse({ status: 200, description: 'Return the magazine.' })
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<MagazineOutputDTO> {
    return this.magazineService.findById(+id);
  }

  @ApiOperation({ summary: 'Create a new magazine' })
  @ApiResponse({
    status: 201,
    description: 'The magazine has been successfully created.',
  })
  @Post()
  async create(@Body() magazine: MagazineInputDTO): Promise<MagazineOutputDTO> {
    return this.magazineService.create(magazine);
  }

  @ApiOperation({ summary: 'Update a magazine by ID' })
  @ApiResponse({
    status: 200,
    description: 'The magazine has been successfully updated.',
  })
  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() magazine: MagazineInputDTO,
  ): Promise<MagazineOutputDTO> {
    return this.magazineService.update(+id, magazine);
  }

  @ApiOperation({ summary: 'Delete a magazine by ID' })
  @ApiResponse({
    status: 204,
    description: 'The magazine has been successfully deleted.',
  })
  @Delete(':id')
  async remove(@Param('id') id: string): Promise<void> {
    await this.magazineService.softDelete(+id);
  }
}
