import { Module } from '@nestjs/common';
import { MagazineController } from './controllers/magazine.controller';
import { MagazineService } from './services/magazine.service';
import { MagazineEntity } from 'src/infrastructure/db/entities/magazine.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([MagazineEntity])],
  controllers: [MagazineController],
  providers: [MagazineService],
})
export class MagazineModule {}
