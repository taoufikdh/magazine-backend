// src/magazine/magazine.service.ts
import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MagazineOutputDTO } from '../dtos/response/magazine.output.dto';
import { MagazineEntity } from 'src/infrastructure/db/entities/magazine.entity';
import { Repository } from 'typeorm';
import { MagazineInputDTO } from '../dtos/requests/magazine.input.dto';

@Injectable()
export class MagazineService {
  constructor(
    @InjectRepository(MagazineEntity)
    private magazineRepository: Repository<MagazineEntity>,
  ) {}

  async findAll(): Promise<MagazineOutputDTO[]> {
    try {
      return this.magazineRepository.find();
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async findById(id: number): Promise<MagazineOutputDTO> {
    try {
      return this.magazineRepository.findOne({ where: { id } });
    } catch (error) {
      throw new NotFoundException();
    }
  }

  async create(magazine: MagazineInputDTO): Promise<MagazineOutputDTO> {
    try {
      return this.magazineRepository.save(magazine);
    } catch (error) {
      throw new InternalServerErrorException();
    }
  }

  async update(
    id: number,
    magazine: MagazineInputDTO,
  ): Promise<MagazineOutputDTO> {
    try {
      await this.magazineRepository.update(id, magazine);
      return this.findById(id);
    } catch (error) {
      throw new InternalServerErrorException();
    }
  }

  async softDelete(id: number): Promise<void> {
    try {
      const deleteResponse = await this.magazineRepository.softDelete(id);
      if (!deleteResponse.affected) {
        throw new NotFoundException();
      }
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }
}
