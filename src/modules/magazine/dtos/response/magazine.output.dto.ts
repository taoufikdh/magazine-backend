import { ApiProperty } from '@nestjs/swagger';

export class MagazineOutputDTO {
  @ApiProperty()
  id: number;

  @ApiProperty()
  title: string;

  @ApiProperty()
  price: number;
}
