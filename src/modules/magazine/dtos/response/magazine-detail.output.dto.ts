import { MagazineOutputDTO } from './magazine.output.dto';

export class MagazineDetailOutputDTO extends MagazineOutputDTO {
  subscribers: any[];
}
