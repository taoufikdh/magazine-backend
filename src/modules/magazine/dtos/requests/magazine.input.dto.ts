import { ApiProperty } from '@nestjs/swagger';

export class MagazineInputDTO {
  @ApiProperty()
  title: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  price: number;
}
